#include "catch/catch.hpp"

#include "netio/netio.hpp"

using namespace netio;


TEST_CASE( "send and receive using zero-copy low-latency sockets", "[zerocopy]")
{
    int port = get_free_port();

    context ctx("posix");
    low_latency_send_socket send(&ctx);
    recv_socket recv(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));

    send.connect(endpoint("127.0.0.1", port));
    send.send(message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}

TEST_CASE( "send and receive from a zero-copy buffered socket", "[zerocopy]")
{
    int port = get_free_port();

    context ctx("posix");
    buffered_send_socket send(&ctx);
    recv_socket recv(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));

    send.connect(endpoint("127.0.0.1", port));
    send.send(message((uint8_t*)"hello, world!", 14));
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == 14 );
}

TEST_CASE( "create a low-latency zero-copy send socket", "[zerocopy]" )
{
    context ctx("posix");
    low_latency_send_socket socket(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));

    REQUIRE( true );
}


TEST_CASE( "send and receive a large message from a zero-copy low-latency socket", "[zerocopy]")
{
    int port = get_free_port();

    context ctx("posix");
    low_latency_send_socket send(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));
    recv_socket recv(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));

    const size_t SIZE = 40000;
    char* d = new char[SIZE];

    send.connect(endpoint("127.0.0.1", port));
    send.send(message((uint8_t*)d, SIZE));
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
}


TEST_CASE( "create a buffered zerocopy send socket", "[zerocopy]" )
{
    context ctx("posix");
    buffered_send_socket socket(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));

    REQUIRE( true );
}


TEST_CASE( "send and receive large message from a zerocopy buffered socket", "[zerocopy]" )
{
    int port = get_free_port();

    context ctx("posix");
    buffered_send_socket send(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));
    recv_socket recv(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));

    const size_t SIZE = 40000;
    char* d = new char[SIZE];
    REQUIRE( d != NULL );
    message m((uint8_t*)d, SIZE);
    CAPTURE(m.size());

    send.connect(endpoint("127.0.0.1", port));
    send.send(m);
    send.flush();
    ctx.event_loop()->run_for(1000);

    message received;
    recv.recv(received);
    send.disconnect();

    REQUIRE( received.size() == SIZE );
    delete[] d;
}


TEST_CASE( "connect to a zerocopy publish socket", "[zerocopy]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));
    subscribe_socket sub(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(0, endpoint("127.0.0.1", port));

    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == true );
}


TEST_CASE( "connect to a publish socket using zerocopy low-latency subscribe", "[zerocopy]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));
    low_latency_subscribe_socket sub(&ctx, [](endpoint&,
    message&) {}, sockcfg::cfg()(sockcfg::ZERO_COPY));

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(0, endpoint("127.0.0.1", port));

    ctx.event_loop()->run_for(1000);
    REQUIRE( subscribed == true );
}



TEST_CASE( "publish with a zerocopy publish socket", "[zerocopy]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, partial_sort, sockcfg::cfg()(sockcfg::ZERO_COPY));
    subscribe_socket sub(&ctx, sockcfg::cfg()(sockcfg::ZERO_COPY));

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(5, endpoint("127.0.0.1", port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);

    message received;
    sub.recv(received);

    REQUIRE( subscribed == true );
    REQUIRE( received.size() == 14 );
}

TEST_CASE( "publish with a zerocopy publish socket - low-latency", "[zerocopy]" )
{
    int port = get_free_port();

    context ctx("posix");
    publish_socket pub(&ctx, port, sockcfg::cfg()(sockcfg::ZERO_COPY));

    bool message_received = false;
    low_latency_subscribe_socket sub(&ctx,
                                     [&message_received](endpoint&, message&)
    {
        message_received = true;
    }, sockcfg::cfg()(sockcfg::ZERO_COPY));

    bool subscribed = false;
    pub.register_subscribe_callback([&](tag, endpoint)
    {
        subscribed = true;
    });

    sub.subscribe(5, endpoint("127.0.0.1", port));
    ctx.event_loop()->run_for(500);
    pub.publish(5,  message((uint8_t*)"hello, world!", 14));
    ctx.event_loop()->run_for(5000);

    REQUIRE( subscribed == true );
    REQUIRE( message_received == true );
}
