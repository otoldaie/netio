#!/bin/bash

# This script will take a command line and call it 1000 times. This is
# useful to run test programs and discover bugs that only occur
# sporadically.
#
# Example:
#     test/run_1000_times.sh  netio_test

set -e

for i in `seq 1 1000`;
  do
    "$@"
  done    
