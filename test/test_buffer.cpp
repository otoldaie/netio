#include "catch/catch.hpp"

#include "netio/netio.hpp"

using namespace netio;


TEST_CASE( "create a buffer feeder and pop a single buffer", "[buffer]" )
{
    context ctx("posix");
    buffer_feeder feeder(128, 256, &ctx);
    reusable_buffer* b;
    REQUIRE( feeder.try_pop(&b) == true );

    REQUIRE( feeder.num_total_buffers() == 128 );
    REQUIRE( feeder.num_available_buffers() == 127 );

    REQUIRE( b->buffer()->pos() == 0 );
    REQUIRE( b->buffer()->size() == 256 );

    feeder.release(b);
    REQUIRE( feeder.num_available_buffers() == 128 );
}


TEST_CASE( "release a reusable_buffer", "[buffer]" )
{
    context ctx("posix");
    buffer_feeder feeder(128, 256, &ctx);
    reusable_buffer* b;
    REQUIRE( feeder.try_pop(&b) == true );

    REQUIRE( feeder.num_available_buffers() == 127 );
    b->release();
    REQUIRE( feeder.num_available_buffers() == 128 );
}


TEST_CASE( "signal while releasing a reusable_buffer", "[buffer]" )
{
    context ctx("posix");
    buffer_feeder feeder(128, 256, &ctx);
    reusable_buffer* b;
    REQUIRE( feeder.try_pop(&b) == true );

    bool called = false;
    feeder.register_buf_available_cb([](void* data)
    {
        *((bool*)data) = true;
    }, &called);

    b->release();
    ctx.event_loop()->run_for(1000);

    REQUIRE( called == true );
}
