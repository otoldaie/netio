#include "catch/catch.hpp"

#include "netio/netio.hpp"

using namespace netio;


TEST_CASE( "Create a default endpoint", "[endpoint]" )
{
    endpoint ep;

    REQUIRE( ep.address() == "" );
    REQUIRE( ep.port() == 0 );
}


TEST_CASE( "Create an IPv4 endpoint", "[endpoint]" )
{
    int port = get_free_port();

    endpoint ep("1.2.3.4", port);

    REQUIRE( ep.address() == "1.2.3.4" );
    REQUIRE( ep.port() == port );
}

TEST_CASE( "Create an IPv6 endpoint", "[endpoint]" )
{
    int port = get_free_port();

    endpoint ep("6f:ab:10::1", port);

    REQUIRE( ep.address() == "6f:ab:10::1" );
    REQUIRE( ep.port() == port );
}

TEST_CASE( "Create an endpoint with 0 address", "[endpoint]" )
{
    int port = get_free_port();

    endpoint ep("0.0.0.0", port);

    REQUIRE( ep.address() == "0.0.0.0" );
    REQUIRE( ep.port() == port );
}
