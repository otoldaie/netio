#include "catch/catch.hpp"
#include "netio/netio.hpp"

#include <string>

using namespace netio;

TEST_CASE( "Create message from string", "[message]" )
{
    std::string test_str("test");
    message m(test_str);
    std::vector<uint8_t> d = m.data_copy();

    REQUIRE( m.size() == test_str.size() );
    REQUIRE( memcmp(d.data(), test_str.data(), test_str.size()) == 0 );
}

TEST_CASE( "Copy message", "[message]" )
{
    std::string test_str("test");
    message m((uint8_t*)test_str.c_str(), test_str.size());

    message m2(m);
    auto d = m2.data_copy();
    REQUIRE( m2.size() == test_str.size() );
    REQUIRE( memcmp(d.data(), test_str.data(), test_str.size()) == 0 );

    message m3 = m;
    auto d3 = m3.data_copy();
    REQUIRE( m3.size() == test_str.size() );
    REQUIRE( memcmp(d3.data(), test_str.data(), test_str.size()) == 0 );
}

TEST_CASE( "Move message", "[message]")
{
    std::string test_str("test");
    message m(test_str);

    message m2(std::move(m));
    auto d = m2.data_copy();
    REQUIRE( m2.size() == test_str.size() );
    REQUIRE( memcmp(d.data(), test_str.data(), test_str.size()) == 0 );
    REQUIRE( m.size() == 0 );
}

TEST_CASE( "message with some fragments", "[message]")
{
    std::string test_str("test");
    message m(test_str);
    REQUIRE( m.num_fragments() == 1 )	;

    char const * d[3] = { "xxx", "yyyy", "zzzzz" };
    size_t s[3] = { 3, 4, 5};
    message m2((uint8_t**)d, s, 3);
    REQUIRE( m2.num_fragments() == 3);
    REQUIRE( memcmp(m2.fragment_list()->data[0], d[0], s[0]) == 0 );
    REQUIRE( m2.fragment_list()->size[0] == s[0] );
    REQUIRE( memcmp(m2.fragment_list()->data[1], d[1], s[1]) == 0 );
    REQUIRE( m2.fragment_list()->size[1] == s[1] );
    REQUIRE( memcmp(m2.fragment_list()->next->data[0], d[2], s[2]) == 0 );
    REQUIRE( m2.fragment_list()->next->size[0] == s[2] );

    REQUIRE( m2.fragment_list()->next->data[1] == nullptr );
    REQUIRE( m2.fragment_list()->next->size[1] == 0 );
    REQUIRE( m2.fragment_list()->next->next == nullptr );
}

TEST_CASE( "message with many fragments", "[message]")
{
    std::vector<const uint8_t*> d_vec;
    std::vector<size_t> s_vec;
    for(int i=0; i < 64; i++)
    {
        size_t s = 16 + i;
        uint8_t* d = new uint8_t[s];
        for(size_t j=0; j<s; j++)
            d[j] = 'a'+i;
        d_vec.push_back(d);
        s_vec.push_back(s);
    }

    message m(d_vec, s_vec);
    REQUIRE( m.num_fragments() == 64);
    int i=0;
    for(const message::fragment* p = m.fragment_list(); p != nullptr; p = p->next)
    {
        REQUIRE( memcmp(p->data[0], d_vec[i], s_vec[i]) == 0 );
        ++i;

        if (p->data[1])
            REQUIRE( memcmp(p->data[1], d_vec[i], s_vec[i]) == 0 );
        ++i;
    }

    auto d = m.data_copy();
    REQUIRE( d[0] == 'a' );
    REQUIRE( m.size() == (64*16 + 63*64/2) ); // yay, math!
    REQUIRE( (char)d[m.size()-1] == (char)('a'+63) );
}


class MyCleanupHook
{
public:
    bool& called;
    MyCleanupHook(bool& b) : called(b) {}

    void operator()(message&)
    {
        called = true;
    }
};

bool callback_called;
void my_cleanup_hook(message&)
{
    callback_called = true;
}


TEST_CASE( "message with cleanup hook", "[message]" )
{
    std::string test_str("test");
    callback_called = false;
    {
        // callback when m goes out of scope
        message m((uint8_t*)test_str.c_str(), test_str.size(), nullptr, my_cleanup_hook);
    }
    REQUIRE( callback_called == true );
}

TEST_CASE( "serialize message to user buffer", "[message]" )
{
    std::string test("hello world!");
    callback_called = false;
    message m((uint8_t*)test.c_str(), test.size(), nullptr, my_cleanup_hook);

    std::vector<char> buf(test.size());

    m.serialize_to_usr_buffer(buf.data());

    REQUIRE( memcmp(buf.data(), test.data(), test.size()) == 0 );
    REQUIRE( callback_called == true);
    REQUIRE( m.num_fragments() == 1);
    REQUIRE( (char*)m.fragment_list()->data[0] == buf.data() ); //should point to the same mem location
}


TEST_CASE( "Add second fragment", "[message]" )
{
    std::string test("hello");
    std::string test2(" world!");
    message m((uint8_t*)test.c_str(), test.size());

    m.add_fragment((uint8_t*)test2.data(), test2.size());

    REQUIRE( m.size() == 12);
    REQUIRE( m.num_fragments() == 2 );
}

TEST_CASE( "Add first fragment", "[message]" )
{
    std::string test("hello world!");
    message m;

    m.add_fragment((uint8_t*)test.data(), test.size());

    REQUIRE( m.size() == 12);
    REQUIRE( m.num_fragments() == 1);
}


#include <mutex>
#include <atomic>
#include <thread>
#include <iostream>
#include "utility.hpp"
#include <condition_variable>

const int TEN_MILLION = 10000000;

void task_1(void)
{
    char const * d[1] = { "Hello, World!" };
    size_t s[1] = { 13 };

    for(unsigned i=0; i<TEN_MILLION; ++i)
    {
        message m((uint8_t**)d, s, 1);
    }
}

void task_2(void)
{
    char const * d[2] = { "Hello, ", "World!" };
    size_t s[2] = { 7, 6 };

    for(unsigned i=0; i<TEN_MILLION; ++i)
    {
        message m((uint8_t**)d, s, 2);
    }
}

void task_3(void)
{
    char const * d[3] = { "Hello, ", "Cruel ", "World!" };
    size_t s[3] = { 7, 6, 6 };

    for(unsigned i=0; i<TEN_MILLION; ++i)
    {
        message m((uint8_t**)d, s, 3);
    }
}

int speed_test(
    void (*callback)(),
    int milliseconds,
    int threads = 1)
{
    int rc = 0;

    Timer tmr;

    std::mutex mtx;
    std::condition_variable cv;
    std::atomic<int> counter(0);
    std::thread *t = new std::thread[threads];

    tmr.reset();
    for(int i=0; i<threads; ++i)
    {
        counter++;
        t[i] = std::thread([&callback, &mtx, &cv, &counter]()
        {
            (callback)();
            counter--;
            cv.notify_one();
        });
    }

    {
        std::unique_lock<std::mutex> lck(mtx);
        if( !cv.wait_for(lck,std::chrono::milliseconds(milliseconds),[&counter]()
    {
        return counter==0;
    }) )
        {
            rc = -1;
        }
    }

    for(int i = 0; i<threads; i++)
    {
        t[i].join();
    }

    std::cout << "elapsed time: "
              << tmr.elapsed() * 1000.0
              << "ms\n";

    delete[] t;
    return rc;
}

TEST_CASE( "Speed test", "[message]")
{
    REQUIRE( (speed_test(task_1, 250)? false: true) );
    REQUIRE( (speed_test(task_2, 250)? false: true) );
    REQUIRE( (speed_test(task_3, 900)? false: true) );
}
