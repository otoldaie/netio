
# Change Log

## v0.8.0 2017-08-22
### Added
* New zerocopy backend
* New message datatype
* New endpoint datatype

### Changed
* Various bugfixes

## v0.7.1 2017-04-21
* Made fi_verbs backend optional

## v0.7.0 2017-02-22
### Note
* Versions got a bit mixed up with the transition to the new FELIX build
  system. To ensure unique versions we are continuing with v0.7.

### Added
* Added netio_cat tool

### Removed
* Removed netio_pubsub, which is no longer needed

### Changed
* Improvements the fi_verbs backend, e.g. windowing

### Fixes
* Many stability improvements to the fi_verbs backend
* Fixed a memory leak in netio::message
* Fixed a race condition in the POSIX backend

## v0.4.5 2016-09-09
### Fixes
* Limit the number of receive pages (patch submitted by Gordon Crone)

## v0.4.4 2016-06-23
### Fixes
* fi_verbs: Add a counter for outstanding completions and wait for completions
  to finish in disconnect()
* netio_throughput: do not wait after sending messages

## v0.4.3 2016-06-22
### Fixes
* Fix fi_verbs backend: Use fi_cq_read instead of fi_cq_sread

## v0.4.2 2016-06-21
### Fixes
* Fix eventloop run_one issue introduced in 0.4.1
* Revert limit on maximum number of pages (leads to blocking in fi_verbs backend)

## v0.4.1 2016-06-21
### Fixes
* Flush timer in buffered sockets does not fire continuously anymore

## v0.4.0 2016-06-15
### Added
* New Infiniband backend based on libfabric

### Changed
* Small improvements (performance and stability) in POSIX backend.


## v0.3.0 2016-02-16

This release is the first release with a re-designed API.

### Added
* Different socket types: buffered, low-latency, publish/subscribe

### Changed
* Only a single namespace "netio"
* Consistent naming conventions


## v0.2.0 2015-11-13

An initial, experimental release for use in S/W ROD studies
